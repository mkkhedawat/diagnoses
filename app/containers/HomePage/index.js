/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import Button from 'react-bootstrap/lib/Button';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import axios from 'axios';

// import H2 from 'components/H2';
// import ReposList from 'components/ReposList';
// import AtPrefix from './AtPrefix';
// import CenteredSection from './CenteredSection';
// import Form from './Form';
// import Input from './Input';
// import Section from './Section';
// import messages from './messages';
import { loadDiagnosesData } from './actions';
// import { changeUsername } from './actions';
// import { makeDiagnosesData } from './selectors';
import reducer from './reducer';
import saga from './saga';
import ListModal from '../../components/ListModal';
import EditModal from '../../components/EditModal';
import './index.scss';

let cxt;
export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */

  constructor(props) {
    super(props);
    cxt = this;
    window.home = this;
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      showListModal: false,
      showEditModal: false,
      diagnosesData: [],
    };
  }

  componentDidMount() {
    cxt.getDiagnosesData();
  }

  getDiagnosesData() {
    axios.get('http://dbtest.domacare.fi:5000/diagnoses', {
      params: {
      },
    })
    .then(function (response) {
      console.log(response.data);
      cxt.setState({
        diagnosesData: response.data,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  triggerSave(params) {
    console.log(params);

    /*
not sure what to pass
    */

    axios.post('http://dbtest.domacare.fi:5000/diagnoses', {
      firstName: 'Fred',
      lastName: 'Flintstone'
    })
    .then(function (response) {
      console.log(response);
      cxt.getDiagnosesData();
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  closeListModal() {
    cxt.setState({ showListModal: false });
  }

  openListModal() {
    cxt.setState({ showListModal: true });
  }

  closeEditModal() {
    cxt.setState({ showEditModal: false });
  }

  openEditModal() {
    cxt.setState({ showEditModal: true });
  }

  render() {
    const ListTitle = () => <Button onClick={cxt.openEditModal}> <span className="add-button" >+</span></Button>;
    return (
      <article className="homepage">
        <Helmet>
          <title>Home Page</title>
          <meta name="description" content="" />
        </Helmet>
        <Button bsStyle="primary" onClick={cxt.openListModal}>getData</Button>
        <ListModal
          showModal={cxt.state.showListModal}
          title={ListTitle()}
          triggerHide={cxt.closeListModal}
          data={this.state.diagnosesData}
        />
        <EditModal
          showModal={cxt.state.showEditModal}
          triggerHide={cxt.closeEditModal}
          triggerSave={cxt.triggerSave}
        />
      </article>
    );
  }


}

HomePage.propTypes = {
};

export function mapDispatchToProps(dispatch) {
  return {
    getDiagnosesData: () => {
      dispatch(loadDiagnosesData());
    },
  };
}

const mapStateToProps = createStructuredSelector({
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
