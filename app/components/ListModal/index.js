/**
 * A link to a certain page, an anchor tag
 */

import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropTypes from 'prop-types';

import './index.scss';

export class ListModalComp extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  render() {
    return (<div className="static-modal">
      <Modal show={this.props.showModal} onHide={this.props.triggerHide}>
        <Modal.Header>
          <Modal.Title>Diagnoses {this.props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
            <tbody>
              <tr>
                <th>Date</th>
                <th>Diagnose</th>
                <th>Type</th>
                <th>Notes</th>
                <th>Actuary</th>
              </tr>
              {
                this.props.data.map((d) => {
                  return (
                    <tr key={d.id}>
                      <td>{d.date}</td>
                      <td style={{ color: 'blue' }}>{d.diagnosis}</td>
                      <td>{d.diagnoseType}</td>
                      <td>{d.notice}</td>
                      <td>{d.authorInfo}</td>
                    </tr>
                  );
                })
                }
            </tbody>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.triggerHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    </div>);
  }
}

ListModalComp.propTypes = {
  showModal: PropTypes.bool,
  triggerHide: PropTypes.func,
  title: PropTypes.element,
  data: PropTypes.array,
};

export default ListModalComp;
