/**
 * A link to a certain page, an anchor tag
 */

import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import moment from 'moment';
import { Grid, Row, Col, FormControl } from 'react-bootstrap';

import './index.scss';

let cxt;
export class EditModal extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    cxt = this;
  }

  getInitialState() {
    return {
      startDate: moment(),
      value: '',
      notes: '',
    };
  }

  handleChange(date) {
    cxt.setState({
      startDate: date,
    });
  }

  handleNameChange(e) {
    cxt.setState({ value: e.target.value });
  }

  handleNotesChange(e) {
    cxt.setState({ notes: e.target.value });
  }

  handleSave() {
    cxt.props.triggerSave({
      date: cxt.state.startDate,
      name: cxt.state.value,
      notes: cxt.state.notes,
    });
    cxt.props.triggerHide();
  }

  render() {
    const VerticalSpacer = () => <Col md={12} style={{ height: '30px' }}> </Col>;
    return (<div className="static-modal">
      <Modal show={cxt.props.showModal} onHide={cxt.props.triggerHide}>
        <Modal.Header>
          <Modal.Title>Edit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Grid>
            <Row className="show-grid">
              <Col md={12}>
                <label htmlFor="d-picker"> Date </label>
                <DatePicker
                  id="d-picker"
                  selected={cxt.state.startDate}
                  onChange={cxt.handleChange}
                />
              </Col>
              <VerticalSpacer />
              <Col md={8} xs={11}>
                <label htmlFor="diagnose"> Diagnose </label>
                <FormControl
                  type="text"
                  value={this.state.value}
                  placeholder="Enter text"
                  onChange={this.handleNameChange}
                />
              </Col>
              <VerticalSpacer />
              <Col md={8} xs={11}>
                <label htmlFor="notes"> Notes </label>
                <FormControl
                  type="text"
                  value={this.state.notes}
                  placeholder="Enter text"
                  componentClass="textarea"
                  onChange={this.handleNotesChange}
                />
              </Col>
              <VerticalSpacer />
              <Col xs={11} md={6}>
                <Button onClick={this.props.triggerHide} >Cancel</Button></Col>
              <Col xs={11} md={5}>
                <Button onClick={cxt.handleSave} >Save</Button>
              </Col>
            </Row>
          </Grid>
        </Modal.Body>
      </Modal>
    </div>);
  }
}

EditModal.propTypes = {
  showModal: PropTypes.bool,
  triggerHide: PropTypes.func,
  triggerSave: PropTypes.func
};

export default EditModal;
